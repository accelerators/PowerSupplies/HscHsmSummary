# Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [2.3.1] - 28-09-2023

### Changed
* improved status order to put on top the worst device state

## [2.3.0] - 28-09-2023

### Changed
* did something clever in the reset command

## [2.2.1] - 07-10-2022

### Changed
* Status: Now print the STATUS of HSM that swaped (RUNNING)
  [ACU-811](https://jira.esrf.fr/browse/ACU-811).

## [2.2.0] - 19-09-2022

### Added
* New Commands **UnswapALl**, **RefreshMOSPlateMonitoring** from
  [ACU-811](https://jira.esrf.fr/browse/ACU-811).
* New Attribute **IsMOSPlateMonitoringOn** from
  [ACU-811](https://jira.esrf.fr/browse/ACU-811).

### Changed
* State/Status now take into account new HSM states (ON/STANDBY/OFF/DISABLE...)

## [2.1.0] - 01-08-2022

### Added
* New Commands **On**, **Off**, **Standby**, **Disable** & **Enable** from
  [ACU-773](https://jira.esrf.fr/browse/ACU-773).

## [2.0.0] - 03-06-2021

### Added
* New attributes NoHsm, DQ_Mode, Q_Mode, SO_Mode to ease GUI development

## [1.0.0] - 26-08-2020

* First install in EBS control system
