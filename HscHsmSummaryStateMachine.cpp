/*----- PROTECTED REGION ID(HscHsmSummaryStateMachine.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        HscHsmSummaryStateMachine.cpp
//
// description : State machine file for the HscHsmSummary class
//
// project :     
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2019
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <HscHsmSummary.h>

/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::HscHsmSummaryStateMachine.cpp

//================================================================
//  States   |  Description
//================================================================
//  ON       |  
//  ALARM    |  
//  OFF      |  
//  FAULT    |  
//  UNKNOWN  |  


namespace HscHsmSummary_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_NoHsm_allowed()
 *	Description : Execution allowed for NoHsm attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_NoHsm_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for NoHsm attribute in Write access.
	/*----- PROTECTED REGION ID(HscHsmSummary::NoHsmStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::NoHsmStateAllowed_WRITE

	//	Not any excluded states for NoHsm attribute in read access.
	/*----- PROTECTED REGION ID(HscHsmSummary::NoHsmStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::NoHsmStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_DQ_Mode_allowed()
 *	Description : Execution allowed for DQ_Mode attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_DQ_Mode_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for DQ_Mode attribute in Write access.
	/*----- PROTECTED REGION ID(HscHsmSummary::DQ_ModeStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::DQ_ModeStateAllowed_WRITE

	//	Not any excluded states for DQ_Mode attribute in read access.
	/*----- PROTECTED REGION ID(HscHsmSummary::DQ_ModeStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::DQ_ModeStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_Q_Mode_allowed()
 *	Description : Execution allowed for Q_Mode attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_Q_Mode_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for Q_Mode attribute in Write access.
	/*----- PROTECTED REGION ID(HscHsmSummary::Q_ModeStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::Q_ModeStateAllowed_WRITE

	//	Not any excluded states for Q_Mode attribute in read access.
	/*----- PROTECTED REGION ID(HscHsmSummary::Q_ModeStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::Q_ModeStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_SO_Mode_allowed()
 *	Description : Execution allowed for SO_Mode attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_SO_Mode_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for SO_Mode attribute in Write access.
	/*----- PROTECTED REGION ID(HscHsmSummary::SO_ModeStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::SO_ModeStateAllowed_WRITE

	//	Not any excluded states for SO_Mode attribute in read access.
	/*----- PROTECTED REGION ID(HscHsmSummary::SO_ModeStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::SO_ModeStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_HsmNames_allowed()
 *	Description : Execution allowed for HsmNames attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_HsmNames_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for HsmNames attribute in read access.
	/*----- PROTECTED REGION ID(HscHsmSummary::HsmNamesStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::HsmNamesStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_HsmStates_allowed()
 *	Description : Execution allowed for HsmStates attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_HsmStates_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for HsmStates attribute in read access.
	/*----- PROTECTED REGION ID(HscHsmSummary::HsmStatesStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::HsmStatesStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_IsMOSPlateMonitoringOn_allowed()
 *	Description : Execution allowed for IsMOSPlateMonitoringOn attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_IsMOSPlateMonitoringOn_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for IsMOSPlateMonitoringOn attribute in read access.
	/*----- PROTECTED REGION ID(HscHsmSummary::IsMOSPlateMonitoringOnStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::IsMOSPlateMonitoringOnStateAllowed_READ
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_Reset_allowed()
 *	Description : Execution allowed for Reset attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_Reset_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Reset command.
	/*----- PROTECTED REGION ID(HscHsmSummary::ResetStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::ResetStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_Enable_allowed()
 *	Description : Execution allowed for Enable attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_Enable_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Enable command.
	/*----- PROTECTED REGION ID(HscHsmSummary::EnableStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::EnableStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_Disable_allowed()
 *	Description : Execution allowed for Disable attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_Disable_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Disable command.
	/*----- PROTECTED REGION ID(HscHsmSummary::DisableStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::DisableStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_On_allowed()
 *	Description : Execution allowed for On attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_On_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for On command.
	/*----- PROTECTED REGION ID(HscHsmSummary::OnStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::OnStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_Off_allowed()
 *	Description : Execution allowed for Off attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_Off_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Off command.
	/*----- PROTECTED REGION ID(HscHsmSummary::OffStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::OffStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_Standby_allowed()
 *	Description : Execution allowed for Standby attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_Standby_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Standby command.
	/*----- PROTECTED REGION ID(HscHsmSummary::StandbyStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::StandbyStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_UnswapAll_allowed()
 *	Description : Execution allowed for UnswapAll attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_UnswapAll_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for UnswapAll command.
	/*----- PROTECTED REGION ID(HscHsmSummary::UnswapAllStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::UnswapAllStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : HscHsmSummary::is_RefreshMOSPlateMonitoring_allowed()
 *	Description : Execution allowed for RefreshMOSPlateMonitoring attribute
 */
//--------------------------------------------------------
bool HscHsmSummary::is_RefreshMOSPlateMonitoring_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for RefreshMOSPlateMonitoring command.
	/*----- PROTECTED REGION ID(HscHsmSummary::RefreshMOSPlateMonitoringStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::RefreshMOSPlateMonitoringStateAllowed
	return true;
}


/*----- PROTECTED REGION ID(HscHsmSummary::HscHsmSummaryStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	HscHsmSummary::HscHsmSummaryStateAllowed.AdditionalMethods

}	//	End of namespace
