# HscHsmSummary

Small Tango class to summarize Hot Swap Cubicle (HSC) hot swap manager (HSM)

## Cloning

to clone this project, simply type:

```
git clone git@gitlab.esrf.fr:accelerators/PowerSupplies/HscHsmSummary.git
```

## Documentation 

Pogo generated documentation in **doc_html** folder

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.

#### Toolchain Dependencies

* C++11 compliant compiler.
* CMake 3.0 or greater is required to perform the build.

### Build

Instructions on building the project.

CMake example:

```bash
cd HscHsmSummary
mkdir -p build/<os>
cd build/<os>
cmake ../..
make
```
